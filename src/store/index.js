import Vue from 'vue'
import Vuex from 'vuex'
import requst from '../request'
import util from '../util'

Vue.use(Vuex)

const state = {
  MyAddressList: [],
  MyPatientList: [],
  doctorData: [],
  drugsData: [],
  Archives: [],
  tangNingTongLuo: [],
  tangNiaoBing: [],
  jiangTangYaoFenLei: [],
  tangNiaoBingZhuYi: [],
  Service: [],
  MedicalRecord: []
}

const mutations = {
  SET_ADDRESSLIST: (state, data) => {
    state.MyAddressList = JSON.parse(JSON.stringify(data))
  },
  SET_PATIENTLIST: (state, data) => {
    state.MyPatientList = JSON.parse(JSON.stringify(data))
  },
  SET_DOCTORDATA: (state, data) => {
    state.doctorData = JSON.parse(JSON.stringify(data))
  },
  SET_DRUGSDATA: (state, data) => {
    state.drugsData = JSON.parse(JSON.stringify(data))
  },
  SET_ARCHIVES: (state, data) => {
    state.Archives = JSON.parse(JSON.stringify(data))
  },
  SET_TANGNINGTONGLUO: (state, data) => {
    state.tangNingTongLuo = JSON.parse(JSON.stringify(data))
  },
  SET_TANGNINGBING: (state, data) => {
    state.tangNiaoBing = JSON.parse(JSON.stringify(data))
  },
  SET_JIANGTANGYAOFENLEI: (state, data) => {
    state.jiangTangYaoFenLei = JSON.parse(JSON.stringify(data))
  },
  SET_TANGNIAOBINGZHUYI: (state, data) => {
    state.tangNiaoBingZhuYi = JSON.parse(JSON.stringify(data))
  },
  SET_MEDICALRECORD: (state, data) => {
    state.MedicalRecord = JSON.parse(JSON.stringify(data))
  },
  SET_SERVICE: (state, data) => {
    state.Service = JSON.parse(JSON.stringify(data))
  }
}

const actions = {
  // MyAddressList（请求方法）
  // 获取数据
  requestGetAddressList: (context) => {
    requst.getAddressList()
      .then(res => {
        let list = res.data
        context.commit('SET_ADDRESSLIST', list)
      })
  },
  // 新增
  requestAddAddressList: (context, value) => {
    requst.addAddressList(value)
      .then(res => {
        context.dispatch('requestGetAddressList')
      })
  },
  // 删除
  requestDeleteAddressList: (context, value) => {
    requst.deleteAddressList(value)
      .then(res => {
        context.dispatch('requestGetAddressList')
      })
  },
  // 修改
  requestUpdateAddressList: (context, value) => {
    requst.updateAddressList(value)
      .then(res => {
        context.dispatch('requestGetAddressList')
      })
  },

  // MyPatientList（请求方法）
  // 获取后台收据请求
  requestGetPatientList: (context) => {
    requst.getPatientList()
      .then(res => {
        let list = res.data
        context.commit('SET_PATIENTLIST', list)
      })
  },
  // 新增
  requestAddPatientList: (context, value) => {
    requst.addPatientList(value)
      .then(res => {
        context.dispatch('requestGetPatientList')
      })
  },
  // 修改
  requestUpdatePatientList: (context, value) => {
    requst.updatePatientList(value)
      .then(res => {
        context.dispatch('requestGetPatientList')
      })
  },
  // 删除
  requestDeletePatientList: (context, value) => {
    requst.deletePatientList(value)
      .then(res => {
        context.dispatch('requestGetAddressList')
      })
  },
  // 医生信息获取请求
  utilGetDoctorData: (context) => {
    util.getDoctorData()
      .then(res => {
        let doctor = res.data
        context.commit('SET_DOCTORDATA', doctor)
      })
  },
  // 药品信息获取请求
  utilGetDrugsData: (context) => {
    util.getDrugsData()
      .then(res => {
        let drugs = res.data
        context.commit('SET_DRUGSDATA', drugs)
      })
  },
  // 健康档案信息获取请求
  utilGetArchives: (context) => {
    util.getArchives()
      .then(res => {
        let archives = res.data
        context.commit('SET_ARCHIVES', archives)
      })
  },
  // 科普宣教信息获取请求
  utilGetTangNingTongLuo: (context) => {
    util.getTangNingTongLuo()
      .then(res => {
        let tangNingTongLuo = res.data
        context.commit('SET_TANGNINGTONGLUO', tangNingTongLuo)
      })
  },
  utilGetTangNiaoBing: (context) => {
    util.getTangNiaoBing()
      .then(res => {
        let tangNiaoBing = res.data
        context.commit('SET_TANGNINGBING', tangNiaoBing)
      })
  },
  utilGetJiangTangYaoFenLei: (context) => {
    util.getJiangTangYaoFenLei()
      .then(res => {
        let jiangTangYaoFenLei = res.data
        context.commit('SET_JIANGTANGYAOFENLEI', jiangTangYaoFenLei)
      })
  },
  utilGetTangNiaoBingZhuYi: (context) => {
    util.getTangNiaoBingZhuYi()
      .then(res => {
        let tangNiaoBingZhuYi = res.data
        context.commit('SET_TANGNIAOBINGZHUYI', tangNiaoBingZhuYi)
      })
  },
  // 服务订单获取请求
  utilGetService: (context) => {
    util.getService()
      .then(res => {
        let service = res.data
        context.commit('SET_SERVICE', service)
      })
  },
  // 病历详情获取请求
  utilGetMedicalRecord: (context) => {
    util.getMedicalRecord()
      .then(res => {
        let MedicalRecord = res.data
        context.commit('SET_MEDICALRECORD', MedicalRecord)
      })
  }
}

const store = new Vuex.Store({
  state,
  actions,
  mutations
})

export default store
