import axios from 'axios'

// MyAddressList
const getAddressList = () => axios.get('http://localhost:9000/api/get/MyAddressList', {})
const updateAddressList = (value) => axios.post('http://localhost:9000/api/update/MyAddressList', { value })
const deleteAddressList = (value) => axios.post('http://localhost:9000/api/delete/MyAddressList', { value })
const addAddressList = (value) => axios.post('http://localhost:9000/api/add/addAddress', { value })

// MyPatientList
const getPatientList = () => axios.get('http://localhost:9000/api/get/MyPatientList', {})
const addPatientList = (value) => axios.post('http://localhost:9000/api/add/addPatientList', { value })
const updatePatientList = (value) => axios.post('http://localhost:9000/api/update/MyPatientList', { value })
const deletePatientList = (value) => axios.post('http://localhost:9000/api/delete/MyPatientList', { value })

export default {

  // MyAddressList
  getAddressList,
  updateAddressList,
  deleteAddressList,
  addAddressList,
  // MyPatientList
  getPatientList,
  addPatientList,
  updatePatientList,
  deletePatientList
}
