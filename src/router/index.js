import Vue from 'vue'
import Router from 'vue-router'
import GetInto from '@/pages/GetInto'
import Home from '@/pages/Home'
import Selected from '@/pages/Selected'
import Knowledge from '@/pages/Knowledge'
import Mine from '@/pages/Mine'
import Archives from '@/pages/Archives'
import MedicalRecord from '@/pages/MedicalRecord'
import MyDoctor from '@/pages/MyDoctor'
import MyPatient from '@/pages/MyPatient'
import EditPatient from '@/pages/EditPatient'
import AddPatient from '@/pages/AddPatient'
import MyService from '@/pages/MyService'
import MyDrugs from '@/pages/MyDrugs'
import MyAddress from '@/pages/MyAddress'
import AddAddress from '@/pages/AddAddress'
import EditAddress from '@/pages/EditAddress'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'GetInto',
      component: GetInto
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/Selected',
      name: 'Selected',
      component: Selected
    },
    {
      path: '/Knowledge',
      name: 'Knowledge',
      component: Knowledge
    },
    {
      path: '/Mine',
      name: 'Mine',
      component: Mine
    },
    {
      path: '/Archives',
      name: 'Archives',
      component: Archives
    },
    {
      path: '/MedicalRecord',
      name: 'MedicalRecord',
      component: MedicalRecord
    },
    {
      path: '/MyDoctor',
      name: 'MyDoctor',
      component: MyDoctor
    },
    {
      path: '/MyPatient',
      name: 'MyPatient',
      component: MyPatient
    },
    {
      path: '/EditPatient',
      name: 'EditPatient',
      component: EditPatient
    },
    {
      path: '/AddPatient',
      name: 'AddPatient',
      component: AddPatient
    },
    {
      path: '/MyService',
      name: 'MyService',
      component: MyService
    },
    {
      path: '/MyDrugs',
      name: 'MyDrugs',
      component: MyDrugs
    },
    {
      path: '/MyAddress',
      name: 'MyAddress',
      component: MyAddress
    },
    {
      path: '/AddAddress',
      name: 'AddAddress',
      component: AddAddress
    },
    {
      path: '/EditAddress',
      name: 'EditAddress',
      component: EditAddress
    }
  ]
})
