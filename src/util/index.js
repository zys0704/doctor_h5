import axios from 'axios'

const getDoctorData = () => axios.get('http://localhost:9000/api/get/doctorData', {})
const getDrugsData = () => axios.get('http://localhost:9000/api/get/drugsData', {})
const getArchives = () => axios.get('http://localhost:9000/api/get/Archives', {})
const getTangNingTongLuo = () => axios.get('http://localhost:9000/api/get/tangNingTongLuo', {})
const getTangNiaoBing = () => axios.get('http://localhost:9000/api/get/tangNiaoBing', {})
const getJiangTangYaoFenLei = () => axios.get('http://localhost:9000/api/get/jiangTangYaoFenLei', {})
const getTangNiaoBingZhuYi = () => axios.get('http://localhost:9000/api/get/tangNiaoBingZhuYi', {})
const getService = () => axios.get('http://localhost:9000/api/get/Service', {})
const getMedicalRecord = () => axios.get('http://localhost:9000/api/get/MedicalRecord', {})

export default {
  getDoctorData,
  getDrugsData,
  getArchives,
  getTangNingTongLuo,
  getTangNiaoBing,
  getJiangTangYaoFenLei,
  getTangNiaoBingZhuYi,
  getService,
  getMedicalRecord
}
